CREATE DATABASE BestBuy;

USE BestBuy;

CREATE TABLE computers (
	CompID INT IDENTITY NOT NULL PRIMARY KEY,
	Brand NVARCHAR(10),
	Model NVARCHAR(30),
	ScreenSize NVARCHAR(5),
	Price DECIMAL(9,2),
	RAM NVARCHAR(10),
	HDD NVARCHAR(10),
	"Description" NVARCHAR(1000),
	Color NVARCHAR (20)
);

INSERT INTO computers (Brand, Model, ScreenSize, Price, RAM, HDD, "Description", Color)
VALUES ('Acer','Intel Core i5-8250U" Laptop', '14"',699.99, '8GB', '256GB', 'Reliable and versatile, the Acer 14" laptop is a perfect option for both professionals and students. It features a 1.6 GHz Intel Quad Core i5 processor, 8GB of RAM, 256GB solid state drive, and Full HD 1080p display. Integrated Bluetooth and USB 3.0 connectivity make it easy to connect additional devices and peripherals.', 'Silver '),
		('HP', 'AMD A9-9425 Dual-Core', '14"', 599.99, '4GB', '128GB', 'Enjoy reliable computing performance and effortless portability with this 14" HP laptop, which features a 3.1GHz AMD A9-9425 processor and 4GB of DDR4 RAM. It comes equipped with Bluetooth connectivity and a variety of ports to add your favourite peripherals.', 'Natural Silver'),
		('ASUS', 'AMD Quad-Core R5-2500U', '15.6"',699.99, '8GB', '256GB', 'Enjoy fast and reliable computing everywhere you go with the ASUS VivoBook 15.6" laptop. It features a 2GHz AMD quad-core processor, 8GB of RAM, and a 256GB solid state drive for powerful performance. The 15.6" Full HD display boasts 1920x1080 resolution to deliver a high-quality, immersive multimedia experience.', 'Dark Grey'),
		('Acer', 'Intel Core i7-8550U', '15.6"', 799.99, '12GB', '1TB', 'The Acer Aspire 5 15.6" laptop has all the essentials you need. Equipped with an Intel Core i7 processor, 12GB of RAM, and integrated Wi-Fi and Ethernet, this laptop lets you quickly browse the Internet, manage your finances, or work from home. Its Full HD display presents images in rich colour and clarity.', 'Black'),
		('Apple', 'Intel Core i5 1.8 GHz', '14"', 1199.99, '8GB', '128GB', '', 'Grey'),
		('Apple', 'Intel Core i5 1.8 GHz', '11"', 1899.99, '8GB', '256GB', '', 'Black'),
		('Lenovo', 'Intel Core i5 5th Gen', '16"', 2199.99, '12GB', '480GB', '', 'Black'),
		('Lenovo', 'Intel Core i5-8250U', '15"', 889.99, '12GB', '480GB', '', 'Black'),
		('Dell', 'Core i5 5300u 2.3GHz', '14"', 989.99, '8GB', '256GB', '', 'Black'),
		('Dell', 'Core i5 5300u 2.3GHz', '14"', 2499.99, '12GB', '500GB', '', 'White')


--1- Query all data from the �Computers� table.
SELECT *
	FROM computers	

--2- We are going to buy a laptop, and we have 1200 CAD. Write a query to show all computers between 700 and 1200.
SELECT *
	FROM computers
	WHERE Price >= 700 AND Price <= 1200

--3- We need also to see all �Dell�, �Acer�, and �Lenovo� laptops. (Use IN to write the query).
SELECT *
	FROM computers
	WHERE Brand IN ('Dell', 'Acer', 'Lenovo')

/*4- Show �Model�, �Price�, and reduced price by 10 percent.
* Do not add any column to your table. We need a third column in the result windows which shows prices reduced 
by 10 percent. Name of the column is: �Promotion Price�.*/
SELECT Model, Price, (Price - (0.10*Price)) AS 'Promotion Price'
	FROM computers

/*1- How many computers have a price more than 1700 CAD? Write a query to show the number of computers with a price 
more than 2000 CAD. */
SELECT COUNT(CompID)
	FROM computers
	WHERE Price > 2000

--2-Delete all computers with prices greater than 2000 CAD, and show the remained computers in our table.
DELETE FROM computers
	WHERE Price > 2000

SELECT *
	FROM computers

--1- We need to see all the computer�s information sorted by the price.
SELECT *
	FROM computers
	ORDER BY (Price)

--2- How many rows exits in your table? Find out using a select command (count function).
SELECT COUNT(*)
	FROM computers

--3- Add the second table. Name it �customers� with CusID, First Name, last name, City.
CREATE TABLE customers (
	CusID INT IDENTITY NOT NULL PRIMARY KEY,
	FirstName NVARCHAR(20),
	LastName NVARCHAR(20),
	City NVARCHAR(20)
)

--4- Add five customers to your table.
INSERT INTO customers
VALUES ('Roberta', 'Apezzatto', 'Waterloo'),
	('Maria', 'Ferrari', 'Cambridge'),
	('Alex', 'Martins', 'Kitchener'),
	('Mariana', 'Meneghelo', 'London'),
	('Silvia', 'Silva', 'Waterloo')

--1- Add The third table. Name it �Sales�. The columns are: ID, CusID, CompID, Date, Quantity.
CREATE TABLE sales (
	ID INT IDENTITY NOT NULL PRIMARY KEY,
	CusID INT,
	CompID INT,
	"Date" DATE,
	Quantity INT
)

/*2- Add 10 sales to your table. It is important to use the same ComID and CusID you used in computers 
and customers tables.*/
SELECT *
	FROM computers
SELECT *
	FROM customers

INSERT INTO Sales (CusID, CompID, "Date", Quantity)
VALUES (1, 3, '2018-09-09', 1),
	(2, 4, '2017-05-03', 1),
	(4, 5, '2019-01-25', 1),
	(5, 9, '2015-07-30', 1),
	(3, 9, '2014-09-08', 2),
	(1, 9, '2019-04-05', 1),
	(3, 6, '2012-12-12', 2),
	(3, 1, '2019-04-03', 1),
	(2, 5, '2015-11-05', 1),
	(4, 1, '2000-10-10', 1)

--3- Change the the Ram of all �Dell� computers to 8.
UPDATE computers
	set RAM = '8GB'
	where Brand = 'Dell';

--4- Change the last customer�s last name to �Jackson�.
UPDATE customers
	set LastName = 'Jackson'
	where CusID = 5

--1- We need to know the name of the customers who bought computer with the model of the computer they bought.
SELECT FirstName + ' ' + LastName AS 'Full Name', Model
	FROM customers
	INNER JOIN sales
	ON customers.CusID = sales.CusID
	INNER JOIN computers
	ON sales.CompID = computers.CompID

--2- We also need to know the date and the price of the computers to be displayed with the name and CusID of the customers.
SELECT customers.CusID, FirstName + ' ' + LastName AS 'Full Name',
	"Date", Price
	FROM sales
	INNER JOIN customers
	ON customers.CusID = sales.CusID
	INNER JOIN computers
	ON sales.CompID = computers.CompID

--3- Which computers did not sold at all?
SELECT *
	FROM computers
	LEFT JOIN sales
	ON computers.CompID = sales.CompID
	WHERE sales.CompID IS NULL

--1- Add a new sales to the sale table. Use the current date as the sale date. (Do not forget to use proper CusID and ComID)
INSERT INTO Sales (CusID, CompID, "Date", Quantity)
VALUES (4, 2, GETDATE(), 1)

SELECT *
	FROM sales

--2- Find the first selling date and the last one. Find out the difference between two date values.
SELECT DATEDIFF(day, MIN("Date"), MAX("Date")) as DIF
	FROM sales

/*1- Find the lowest computer price and use it as a subquery to find the customers buying the computer with the lowest 
price.*/
SELECT *
FROM customers, sales
WHERE customers.CusID = sales.CusID
AND sales.CompID IN (SELECT TOP 1 CompID
					FROM computers
					ORDER By Price)

/*2- We need to know if there are customer name having �e� or �a� in their last names. If there are any customers by 
that name, find out if they bought any �Dell� computers?*/
SELECT CusID
	FROM customers
	WHERE (LastName like '%a%' OR LastName like '%e%')
	AND customers.CusID IN (SELECT CusID
							FROM computers, sales
							WHERE computers.CompID = sales.CompID
							AND computers.Brand = 'Dell')
