/*This is the first part of your 10% assignment. 
1- Add your first name and last name to the Salesperson. 
The EmpID have to be 99.
2- How many employees are in the Salesperson table?
3- Delete the last row. (your name)
4- Add your name with EmpID =25
*/

USE Subqueries;



--Inserting into SalesPerson my full name and choosing a EmpId (is it possible to change entitity values)
INSERT INTO SalesPerson (EmpID, First_Name, Last_Name)
VALUES (99, 'Roberta', 'Martins')

SELECT *
FROM SalesPerson
ORDER BY EmpID DESC

--Couting how many employees are in the table using COUNT
SELECT COUNT(EmpId)
FROM SalesPerson

--Deleting the last row that I have created (EmpID = 99)
DELETE FROM SalesPerson
WHERE EmpID = 99

--Now, inserting my name to the next EmpID 
INSERT INTO SalesPerson (EmpID, First_Name, Last_Name)
VALUES (25, 'Roberta', 'Martins')

/*5- update employee number 20. Change His first name to SALMAN.
6- update employee number 20. Change his last name to KHAN.
7- Now  Change his first name and last name with one update query. Change it to �Salman� �Khan�.
8- Increase the number of the employees to 30. Is it possible to do it with only one INSERT INTO 
command? (add 5 more employees).
9- Add your friend�s name as the employee number 25. Is it possible to have two employees with the same EmpID?
10-  Create a report for your in-class assignment and send it as the first part of your 10 percent assignment. 
*/

--Updating employee number 20 to a new first name as SALMAN
UPDATE SalesPerson
SET First_Name = 'SALMAN'
WHERE EmpID = 20

--Now, updating employee number 20 to a new last name as KHAN
UPDATE SalesPerson
SET Last_Name = 'KHAN'
WHERE EmpID = 20

--Updating employee number 20 to a new first name and last name in the same query
UPDATE SalesPerson
SET First_Name = 'Salman', Last_Name = 'Khan'
WHERE EmpID = 20

--Inseting more 5 employees using just one query (setting 5 different values)
INSERT INTO SalesPerson (EmpID, First_Name, Last_Name)
VALUES (26, 'Alex', 'Pinho'), (27, 'Mariana', 'Martins'), 
	(28, 'Luiz', 'Carlos'), (29, 'Marlene', 'Apezzatto'), 
	(30, 'Rogerio', 'Apezzatto')

Select * 
From SalesPerson
ORDER BY EmpID DESC

--Trying to add another emplyee using the empid = 25 (same of mine)
INSERT INTO SalesPerson (EmpID, First_Name, Last_Name)
VALUES (25, 'Sarah', 'Marmed')

/*How many rows are in the Sales.customer table (Name the column�s title as 'Number of Sales'. 
How many unrepeated storeID�s are in the table 
(Name the column�s title as �Number of Stores�?*/
USE AdventureWorks2014

--Counting how many rows are in the Sales.Customer table and giving a name for the colum's title
SELECT COUNT (CustomerID) AS 'Number of Sales'
FROM Sales.Customer

--Counting how many DISTINCT stores are in the Sales.Customer table and giving a name for the colum's title
SELECT COUNT (DIStINCT StoreID) AS 'Number of Stores'
FROM Sales.Customer

/*B2. Show all data in 
A) Production.Product with class = L, 
B) Production.ProductCostHistory with StandardCost less than 5
C) Purchasing.PurchaseOrderDetail with OrderQty more than 5000.
*/
--Showing all data from table product that has class = L
SELECT *
	FROM Production.Product
	WHERE Class = 'L'

--Select all data from ProductCostHistory less than 5 of standard cost and changing all dates for date format
SELECT ProductID, CAST (StartDate AS Date) AS StartDate,
	CAST (EndDate AS Date) AS EndDate, StandardCost,
	CAST (ModifiedDate AS Date) AS ModifiedDate
	FROM Production.ProductCostHistory
	WHERE StandardCost < 5

--Selecting all data from PurchaseOrderDetail with quantity of order more than 5000
SELECT *,
	CAST (DueDate AS date) AS DueDate,
	CAST (ModifiedDate AS Date) AS ModifiedDate
	FROM Purchasing.PurchaseOrderDetail
	WHERE OrderQty > 5000

--Comparing two tables to see which one has the newer modified date using inner join
SELECT	P.ProductID,
		P.StandardCost AS 'Product StandardCost', 
		CAST (P.ModifiedDate as DATE) AS 'Product ModifiedDate',
		PCH.StandardCost AS 'History StandardCost',
		CAST (PCH.ModifiedDate as DATE) AS 'History ModifiedDate'
	FROM Production.ProductCostHistory AS PCH
		JOIN Production.Product AS P
	ON P.ProductID = PCH.ProductID
	ORDER BY P.ModifiedDate, PCH.ModifiedDate DESC

--Comparing two tables to see the differences between products that do not have the same standard cost
SELECT	P.ProductID,
		P.StandardCost AS 'Product StandardCost', 
		PCH.StandardCost AS 'History StandardCost'
	FROM Production.ProductCostHistory AS PCH
		JOIN Production.Product AS P
	ON P.ProductID = PCH.ProductID 
	AND P.StandardCost != PCH.StandardCost

--Getting the average of changes of Standard costs from two tables
SELECT AVG (P.StandardCost - PCH.StandardCost) AS 'AVG StandardCost'
	FROM Production.ProductCostHistory AS PCH
		JOIN Production.Product AS P
	ON P.ProductID = PCH.ProductID 

SELECT *
FROM Purchasing.PurchaseOrderDetail

SELECT *
FROM Production.Product

--showing all the product with �seat Assembly� in their names which 
--have not been purchased yet(PurchaseOrderDetailID IS NULL)
SELECT PO.PurchaseOrderDetailID, P.*
	FROM Production.Product AS P
	LEFT JOIN Purchasing.PurchaseOrderDetail AS PO
	ON PO.ProductID = P.ProductID
	WHERE P.Name LIKE '%Seat Assembly%' 
	AND PurchaseOrderDetailID IS NULL
	ORDER BY P.ProductID

--Showing products grouping by their ProductID and adding their sotcked quantity (showing less to highest)
SELECT ProductID, SUM (StockedQty) AS 'SUM Stocked Qty'
	FROM Purchasing.PurchaseOrderDetail
	GROUP BY ProductID
	ORDER BY SUM (StockedQty) ASC

--Creating a new table from HumanResources.Employee with onle Male employees
SELECT *
	INTO MarriedMaleEmp
	FROM HumanResources.Employee
	WHERE Gender = 'M'

SELECT * FROM MarriedMaleEmp