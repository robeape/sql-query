--1
--selecting information where addressLine2 is not a null value 
SELECT addressID, addressLine1, addressLine2, city
	FROM Person.Address
	WHERE addressLine2 is not null
	ORDER BY city

--counting how many lines we have as returning
SELECT count(*)
	FROM Person.Address
	WHERE addressLine2 is not null

--2
--selecting people who lives in Toronto or Los Angeles and do not have null values
SELECT addressID, addressLine1, addressLine2, city
	FROM Person.Address
	WHERE addressLine2 is not null AND (city = 'Toronto' OR city = 'Los Angeles')

--3
--selecting the same public of question 3 but adding another option of living city -> Downey
SELECT addressID, addressLine1, addressLine2, city
	FROM Person.Address
	WHERE addressLine2 is not null 
	AND  city in ('Toronto', 'Los Angeles','Downey')

--4A
--selecting all the information with standardCost between 100 and 120 and ordering by Modified Date
SELECT ProductID, StandardCost, CAST(StartDate AS date) AS 'StartDate', 
		CAST(EndDate AS date) AS 'EndDate', 
		CAST (ModifiedDate AS date) AS 'ModifiedDate'
	FROM Production.ProductCostHistory
	WHERE standardCost BETWEEN 100 AND 120
	ORDER BY modifiedDate

--4B
--executing the same query of 4A but only with Standard Cost more than 2000
SELECT ProductID, StandardCost, CAST(StartDate AS date) AS 'StartDate', 
		CAST(EndDate AS date) AS 'EndDate', 
		CAST (ModifiedDate AS date) AS 'ModifiedDate'
	FROM Production.ProductCostHistory
	WHERE standardCost > 2000
	ORDER BY modifiedDate

--5
--selecting (maximum 10) persons with lastnames ending with ll and starting with Ru and firstname starting with B.
SELECT TOP 10 BusinessEntityID, FirstName + ' ' + LastName AS 'full name', 
		CAST (ModifiedDate AS date) AS 'ModifiedDate'
	FROM Person.Person
	WHERE RIGHT(LastName,2) = 'll' AND LEFT(LastName,2) = 'Ru' 
		AND LEFT(FirstName,1) = 'B'

--5
--selecting (maximum 10) persons with lastnames ending with ll and starting with Ru and firstname starting with B.
SELECT TOP 10 BusinessEntityID, FirstName, LastName, 
		CAST (ModifiedDate AS date) AS 'ModifiedDate'
	FROM Person.Person
	WHERE LastName LIKE 'Ru%ll' AND FirstName LIKE 'B%'

--6
--selecting (maximum 10) persons with lastnames ending with 'll' and exactly 3 characters in the first name
SELECT TOP 10 BusinessEntityID, FirstName, LastName, 
		CAST (ModifiedDate AS date) AS 'ModifiedDate'
	FROM Person.Person
	WHERE RIGHT(LastName,2) = 'll' AND FirstName LIKE '___'

--6
--selecting (maximum 10) persons with lastnames ending with 'll' and exactly 3 characters in the first name
SELECT TOP 10 BusinessEntityID, FirstName, LastName, 
		CAST (ModifiedDate AS date) AS 'ModifiedDate'
	FROM Person.Person
	WHERE LastName LIKE '%ll' AND FirstName LIKE '___'

--7
--executing the same query as question 6 adding today's date and difference of days since modified date to today
SELECT TOP 10 BusinessEntityID, FirstName, LastName, 
		CAST (ModifiedDate AS date) AS 'ModifiedDate',
		CAST(GETDATE() AS date) AS Today,
		DATEDIFF(day, ModifiedDate, GETDATE()) AS 'Days Past'
	FROM Person.Person
	WHERE RIGHT(LastName,2) = 'll' AND FirstName LIKE '___'
