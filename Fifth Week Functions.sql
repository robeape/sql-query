USE Subqueries

SELECT	[SalesID], 
		[ProductID], 
		[Price],
		[SalesDate],
		[EmpID]
FROM Sales

SELECT DISTINCT ProductID, Price
FROM Sales
ORDER BY ProductID

SELECT EmpID, First_Name, Last_Name
FROM SalesPerson

SELECT COUNT (EmpID)
FROM SalesPerson;

SELECT DISTINCT ProductID
FROM Sales

SELECT COUNT(*) AS NumberOfProduct
FROM (SELECT DISTINCT ProductID FROM Sales) AS Sales;

SELECT COUNT (DISTINCT ProductID)
FROM Sales

SELECT AVG (Price)
FROM Sales

SELECT SalesID, ProductID, Price, SalesDate, EmpID
	FROM Sales
	WHERE Price > (SELECT AVG (Price)
					FROM Sales)
					ORDER BY Price;

SELECT SUM (Price) AS 'Sum',
		MIN (Price) AS Minimum,
		MAX (Price) AS Maximum
	FROM Sales

SELECT EmpID, 
	SUM (Price)
	FROM Sales
	GROUP BY EmpID
	ORDER BY EmpID

--Showing the employee with the highest number of sale
SELECT TOP 1 EmpID, 
	SUM (Price) AS TotalSale
	FROM Sales
	GROUP BY EmpID
	ORDER BY TotalSale DESC

--HAVING is the same os WHERE but is used with GROUP BY
SELECT EmpID, SUM (Price) AS Total
FROM Sales
GROUP BY EmpID
HAVING SUM (Price) > 2000;

--We need to see the full name of the five employees with highest total of the sales prices
SELECT TOP 5 P.First_Name, P.Last_Name, SUM (S.Price) AS TotalSale
	FROM SalesPerson AS P
	INNER JOIN Sales AS S
	ON P.EmpID = S.EmpID
	GROUP BY P.First_Name, P.Last_Name
	ORDER BY TotalSale DESC