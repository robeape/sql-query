--cross join
SELECT top 10 FirstName, LastName, EmailAddress
FROM Person.Person, Person.EmailAddress

--implicit join
SELECT top 10 FirstName, LastName, EmailAddress, P.BusinessEntityID
FROM Person.Person AS P, Person.EmailAddress AS E
WHERE P.BusinessEntityID = E.BusinessEntityID

--explicit join
SELECT top 10 FirstName, LastName, EmailAddress, P.BusinessEntityID
FROM Person.Person AS P INNER JOIN Person.EmailAddress AS E
ON P.BusinessEntityID = E.BusinessEntityID

/*
SELECT ColumnList
FROM LeftTable
JOINTYPE RightTable
ON JoinCondition
*/