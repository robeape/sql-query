/*We need to know the number of products we have in the PurchaseOrderDetail table. 
(count the number of un-repeated productid)*/

--Counting the distincts produts from PurchaseOrderDetail table
SELECT COUNT( DISTINCT ProductID )
	FROM Purchasing.PurchaseOrderDetail

/*Write a query to show the productID of the most profitable product(ignore production costs) 
after price and order quantity are considered (maximum amount of money gained for each product id)
�	Use SUM and group by to get the best result.  
�	HINT: Should be 3358797.75*/

--Showing the ProductID of the most profitable product (with highest LineTotal)
--As we have more rows with the same ProductID, I have to group them and order by DESC
SELECT TOP 1 ProductID, SUM (LineTotal) AS Total
	FROM Purchasing.PurchaseOrderDetail
	GROUP BY ProductID
	ORDER BY SUM (LineTotal) DESC

/*Write a query to show the names of the top 5 most profitable products, as in question 2. 
Remember to take both price and quantity sold into account.
�	You must join two tables.
Purchasing.PurchaseOrderDetail
Production.Product */

--Listing the names (from Production.Product) of 5 most profitable products (the same query of 2nd question)
SELECT TOP 5 P.Name, SUM (LineTotal) AS Total
	FROM Purchasing.PurchaseOrderDetail AS PO,
		Production.Product AS P
	WHERE PO.ProductID = P.ProductID
	GROUP BY P.Name
	ORDER BY SUM (LineTotal) DESC

/*Write a query to show all product ID with the stock quantity less than average stock quantity.
�	You have to use sub query
Purchasing.PurchaseOrderDetail
*/
--Showing ProductID with stock quantity less than average stock quantity (using subquery)
SELECT ProductID, StockedQty
	FROM Purchasing.PurchaseOrderDetail
	WHERE StockedQty < (SELECT AVG (StockedQty)
						FROM Purchasing.PurchaseOrderDetail)

/*We need to know the product id and the modified date of the products with special 
offer �Half-Price Pedal Sale�.
Sales.SpecialOfferProduct 
Sales.SpecialOffer 
*/

--Listing the ProductID and Date of products with description = �Half-Price Pedal Sale�
SELECT SP.ProductID, 
		CAST (SP.ModifiedDate AS date) AS 'ModifiedDate'
	FROM Sales.SpecialOfferProduct AS SP
	INNER JOIN	Sales.SpecialOffer AS SO
	ON SP.SpecialOfferID = SO.SpecialOfferID
	AND SO.Description = 'Half-Price Pedal Sale'

