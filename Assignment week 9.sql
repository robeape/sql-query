/*Update the modified date for all the employees to current date. Employee type is EM.
	Do not forget to use ROLLBACK
	You must use JOIN.
	273 rows must be affected.*/
--Starting transaction to be able to rollback after finishing the exercise.
--Updating the date from person.emailaddress to current date where employees have person type = 'EM' at person.person table
Begin Transaction;
UPDATE Person.Emailaddress
	SET ModifiedDate = GETDATE()
	WHERE BusinessEntityID IN (SELECT BusinessEntityID 
								FROM Person.Person 
								WHERE PersonType = 'EM')

/*Based on the employeeID, using Fetch & Offset show the 3rd twenty rows in the table. (Rows from 40 to 60). 	
Person.EmailAddress
Person.Person*/
--showing modifications of the third twenty rows in the emailaddress table where person type = 'EM' at person table
SELECT *
	from Person.EmailAddress
	where BusinessEntityID IN (SELECT BusinessEntityID 
								FROM Person.Person 
								WHERE PersonType = 'EM')
	order by BusinessEntityID
	OFFSET 39 ROWS FETCH FIRST 21 ROWS ONLY;

--rollback: letting the data as in the begining
rollback;
-- ----------------------------------------------------------------------------

/*Increase the Standard Price by 10 percent for the products with standard prices higher than 50.00.
	(12 row(s) affected)	
Purchasing.ProductVendor*/
--Starting transaction to be able to rollback after finishing the exercise.
--Increasing in 10% the Standard Price that are greater than 50.00
Begin Transaction;
UPDATE Purchasing.ProductVendor
	SET StandardPrice = StandardPrice * 1.10
	WHERE StandardPrice > 50.00
SELECT *
	FROM Purchasing.ProductVendor
	WHERE StandardPrice > 50.00
--rollback: letting the data as in the begining
rollback;
-- ----------------------------------------------------------------------------

/*We want to update the modified date for all the persons without mail promotion and with 
first names starting with Ca to 1th of December 2017.
	You have to use N before the strings, and you have to explain the reason of using it after your screenshots.
	 (633 row(s) affected)	
Person.Person*/
--Starting transaction to be able to rollback after finishing the exercise.
--
Begin Transaction;
UPDATE Person.Person
	SET ModifiedDate = 2017-12-01
	WHERE EmailPromotion = 0
	AND FirstName LIKE 'Ca%'
SELECT *
	FROM Person.Person
	WHERE EmailPromotion = 0
	AND FirstName LIKE 'Ca%'
--rollback: letting the data as in the begining
rollback;
-- ----------------------------------------------------------------------------

/*Using  @MaxOrderQTY variable, update the MaxOrderQTY to 5000 for the seven highest OnOrderQTY. 
	You have to use Declare
	You need to use SubQuery.
	7 rows affected.	
Purchasing.ProductVendor */
--Starting transaction to be able to rollback after finishing the exercise.
--Creating a variable and start it as 5000. Insert the value of this variable to the seven highest OnOrderQTY
Begin Transaction;
DECLARE @MaxOrderQTY int = 5000; 
UPDATE Purchasing.ProductVendor
	SET OnOrderQty = @MaxOrderQTY
	WHERE OnOrderQty IN (SELECT TOP 7 OnOrderQty
							FROM Purchasing.ProductVendor
							ORDER BY OnOrderQty DESC)

--rollback: letting the data as in the begining
rollback;