USE AdventureWorks2014

GO

--log
--security 
CREATE TRIGGER reminder1 
ON HumanResources.Department 
AFTER INSERT, UPDATE 
AS 
     Print ('Insert or Update detected...');
GO 

INSERT INTO HumanResources.Department
VALUES ('New Department2', 'Quality Assurance', GETDATE());

GO

ALTER TRIGGER HumanResources.reminder1 
ON HumanResources.Department 
AFTER INSERT, UPDATE 
AS Print ('We are changing the Text message');

GO

CREATE TRIGGER ddl_trig_database   
ON ALL SERVER   
FOR CREATE_DATABASE   
AS   
    PRINT 'Database Created.'  

GO

CREATE DATABASE test11

GO 

DROP TRIGGER ddl_trig_database  
ON ALL SERVER;  

GO  

DROP DATABASE test11


