-- Choosing the database
USE sis;

-- Selecting contente from payment
SELECT * 
FROM Payment;

-- Selecting specific content from student
SELECT number, balance, balance + 1.13 AS 'cofee added'
FROM Student;

PRINT 'Roberta';

-- Cities names (without repeat a name) in the Person table ordering by descending
SELECT DISTINCT city
FROM Person
ORDER BY city DESC;

-- Selecting first and last name and ordering by two fields
SELECT firstName, lastName
FROM Person
ORDER BY firstName DESC, lastName ASC

-- balances and student number in the Person table where balances are not greater than 0
SELECT number, balance
FROM Student
WHERE NOT balance > 0;

-- balances that are less than 2000 and more than 5000
SELECT number, balance
FROM Student
WHERE balance <= 2000 OR balance >= 5000;
