USE AdventureWorks2014

GO

--controlling the data that someone can see
--views are used for security
--it is possible to update data unless we make some restrictions

CREATE VIEW SimplePerson1
AS
SELECT TOP 20 BusinessEntityID, Title, FirstName, LastName
	FROM Person.Person
	WHERE Title is not null;

GO

SELECT *
FROM SimplePerson1;

GO

CREATE VIEW SimplePerson
AS
SELECT TOP 20 BusinessEntityID, Title, FirstName, LastName
	FROM Person.Person
	ORDER BY FirstName;

GO
--to see the design of the view
SP_helptext SimplePerson;

DROP VIEW SimplePerson, SimplePerson1

GO

CREATE VIEW hiredate_view
AS
SELECT p.FirstName, p.LastName, e.BusinessEntityID, e.HireDate
	FROM HumanResources.Employee AS e
	JOIN Person.Person AS p ON e.BusinessEntityID = p.BusinessEntityID

GO
/****** Script for SelectTopNRows command from SSMS  ******/
SELECT [FirstName]
      ,[LastName]
      ,[BusinessEntityID]
      ,[HireDate]
  FROM [AdventureWorks2014].[dbo].[hiredate_view]

GO

--with encryption: users cannot anything inside the view because they cannot see the view anymore
CREATE VIEW Purchasing.PurchaseOrderReject
WITH ENCRYPTION
AS
SELECT PurchaseOrderID, ReceivedQty, RejectedQty,
		RejectedQty / ReceivedQty AS RejectRatio, DueDate
	FROM Purchasing.PurchaseOrderDetail
	WHERE RejectedQty / ReceivedQty > 0
	AND DueDate > CONVERT(DATETIME, '20010630', 101);

GO

SP_helptext "Purchasing.PurchaseOrderReject";

