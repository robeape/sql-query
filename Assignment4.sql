/*1.	If you take a look at the Product table in the Production schema, so (Production.Product) you will 
see a large selection of products that the company sells. Now look at the Production.ProductReview table. 
This table lists all the products that customers have written reviews for. (Amazon will ask you to review 
each product once you purchase it).  I would like you to come up with a SQL query that will list the Name, 
ProductReviewID and Color of each product that has a review written for it and is not of the color Multi 
or Yellow.

Hint: Now there are only 4 products that have ratings associated with them, so as a hint, you result set should 
be just 3 rows or records.*/

SELECT P.Name, R.ProductReviewID, P.Color
	FROM Production.Product AS P
	INNER JOIN Production.ProductReview AS R
	ON  P.ProductID = R.ProductID
	AND P.Color != 'Multi' 
	AND P.Color != 'Yellow'

/*2.	 Consider a join between the SalesTerritory table and the SalesPerson table on their TerritoryID 
columns. A basic join should show the any territory that has been assigned to a sales person.

SELECT *
FROM Sales.SalesTerritory AS T
JOIN Sales.SalesPerson AS P
ON T.TerritoryID = P.TerritoryID

However I would like you to produce SQL that would display 3 columns, the Territory name, the BusinessEntityID 
and the Year to Date sales numbers. Add in filters to show only rows where the YearToDate numbers are over $1 
Million and are in the US region and the percentage of commission given to the BusinessEntityID is less than 
10%.
Hint: you should end up with 8 rows.*/

SELECT T.Name, P.BusinessEntityID, T.SalesYTD
	FROM Sales.SalesTerritory AS T
	INNER JOIN Sales.SalesPerson AS P
	ON T.TerritoryID = P.TerritoryID
	AND T.SalesYTD > 1000000
	AND CountryRegionCode = 'US'
	AND CommissionPct < 0.1

/*3.	Expand the above query to also include the sales person�s first and last names.*/

SELECT PP.FirstName, PP.LastName, T.Name, P.BusinessEntityID, T.SalesYTD
	FROM Sales.SalesTerritory AS T
	INNER JOIN Sales.SalesPerson AS P
	ON T.TerritoryID = P.TerritoryID
	INNER JOIN Person.Person AS PP
	ON P.BusinessEntityID = PP.BusinessEntityID
	AND T.SalesYTD > 1000000
	AND CountryRegionCode = 'US'
	AND CommissionPct < 0.1

/*4.	 Build a query that will return the FirstName, LastName, BusinessEntityID, Title and NationalIDNumber 
of all the employees in the company. Your query should return 290 rows.*/

-- selecting information of all employees in the company matching them by BusinessEntityID
SELECT P.Title, P.FirstName, P.LastName, P.BusinessEntityID, E.NationalIDNumber
	FROM Person.Person AS P
	INNER JOIN HumanResources.Employee AS E
	ON P.BusinessEntityID = E.BusinessEntityID