USE sis

SELECT *
FROM StudentOffence;

SELECT studentNumber
FROM StudentOffence
WHERE penaltyCode = 'A';

SELECT C.studentNumber, C.finalMark
FROM CourseStudent AS C;

SELECT C.studentNumber, C.finalMark
FROM CourseStudent AS C
INNER JOIN StudentOffence AS S
ON C.studentNumber = S.studentNumber
WHERE S.penaltyCode = 'A';

--I can only use "=" in my subquery if my second table has only 1 row
SELECT studentNumber, finalMark
FROM CourseStudent
WHERE studentNumber = (SELECT studentNumber
							FROM StudentOffence
							WHERE penaltyCode = 'A');

--So, instead of using "=", use "IN"
SELECT studentNumber, finalMark
FROM CourseStudent
WHERE studentNumber IN (SELECT studentNumber
							FROM StudentOffence
							WHERE penaltyCode = 'A');

--The same for "!=", use "NOT IN"
SELECT studentNumber, finalMark
FROM CourseStudent
WHERE studentNumber NOT IN (SELECT studentNumber
							FROM StudentOffence
							WHERE penaltyCode = 'A');

--What are the names of the employees who work in the School of business? (Tables Person and Employee)
SELECT lastName, firstName
FROM Person
WHERE number IN (SELECT number
					FROM Employee
					WHERE schoolCode = 'BUS');

--the same query but using INNER JOIN
SELECT P.lastName, P.firstName
FROM Person AS P
INNER JOIN Employee AS E
ON P.number = E.number
WHERE E.schoolCode = 'BUS';

--What are the names of the employees who work in the School of Business? (ANY/EXISTS)
SELECT lastName, firstName
FROM Person
WHERE number = ANY (SELECT number
					FROM Employee
					WHERE schoolCode = 'BUS');

SELECT lastName, firstName
FROM Person
WHERE EXISTS (SELECT number
				FROM Employee
				WHERE Person.number = Employee.number
				AND schoolCode = 'BUS');

--Which employees work in 2A139 in the school of business?
SELECT lastName, firstName
FROM Person
WHERE EXISTS (SELECT number
				FROM Employee
				WHERE Person.number = Employee.number
				AND schoolCode = 'BUS'
				AND location = '2A139');

SELECT lastName, firstName
FROM Person
WHERE number = ANY (SELECT number
					FROM Employee
					WHERE schoolCode = 'BUS'
					AND location = '2A139');

