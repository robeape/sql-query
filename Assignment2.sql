--  Query full names and birthdates and sort them by their birthdates. 
SELECT firstName, lastName, birthdate
FROM Person
ORDER BY birthdate;

-- We need to get the postal code of the person with number: 7572381
SELECT postalCode
FROM Person
WHERE number = 7572381;

-- How many students have NULL balance? * (You need to do a little search to find out how to find NULL values)
SELECT count(*) 
FROM Student 
WHERE balance IS NULL ;

-- Write a query to show number, local city, and balance of the students with negative amount in their balance. 
SELECT number, localCity, balance 
FROM Student 
WHERE balance < 0; 

/* Write a query to show number, local city, and balance of the students with negative amount in 
their balance and sort them by their balances from highest value to the lowest.*/
SELECT number, localCity, balance 
FROM Student 
WHERE balance < 0 
ORDER BY balance DESC; 

/*Write a query to show number, local city, and balance of the students with negative amount in their balance 
and sort them by their balances from highest value to the lowest. Change the Title to �Student ID�, City, and Dept.*/
SELECT number AS 'Student ID', localCity AS 'City', balance AS 'Dept'
FROM Student
WHERE balance < 0
ORDER BY balance DESC;

/*Write a query to show the first name, last name, alternate phone number, and country code of the persons who 
have a Null value in their alternate phone numbers and their country code is not �CAN�. (You need to do a little 
search to find out how to find NULL values)*/


/*Write a query to show the first name, last name, alternate phone number, and country code of the persons who 
have a Null value in their alternate phone numbers and their country code is not �USA� or �CHN�.*/
SELECT firstName, lastName, alternatePhone, countryCode 
FROM person 
WHERE alternatePhone IS NULL AND (countryCode != 'USA' AND countryCode != 'CHN');