UPDATE Sales.SalesReason 
SET Name = 'N/A' 
WHERE SalesReasonID = 10;
-- ----------------------------------------------------------------------------
UPDATE HumanResources.Department 
SET Name = Name +' Europe' 
WHERE DepartmentID = 19 
  AND NAME NOT LIKE '% Europe';
-- ----------------------------------------------------------------------------
--Look at the table�s data
SELECT Bonus, CommissionPct, SalesQuota 
FROM Sales.SalesPerson;
--Do not forget to add a start point
Begin Transaction;
--Update your table
UPDATE Sales.SalesPerson  
SET Bonus = 6000, CommissionPct = .10, SalesQuota = NULL;  
--Look at your data again
SELECT Bonus, CommissionPct, SalesQuota 
FROM Sales.SalesPerson;
--rollback before the beginning point
rollback;
--Look at your data again
SELECT Bonus, CommissionPct, SalesQuota 
FROM Sales.SalesPerson;
-- ----------------------------------------------------------------------------
Begin Transaction;
select * from Production.Product
GO 
UPDATE Production.Product 
SET Color = N'Metallic Red' 
WHERE Name LIKE N'Road-250%' AND Color = N'Red';
rollback;
-- ----------------------------------------------------------------------------
SELECT [BusinessEntityID]
      ,[FirstName]
      ,[LastName]
  FROM [AdventureWorks2014].[Person].[Person]
  ORDER BY [BusinessEntityID]
  OFFSET 1000 ROWS FETCH FIRST 1000 ROWS ONLY;
-- ----------------------------------------------------------------------------
SELECT *
	FROM HumanResources.Employee;
Begin Transaction;
UPDATE TOP (10) HumanResources.Employee 
SET VacationHours = VacationHours * 1.25;
rollback;
-- ----------------------------------------------------------------------------
begin transaction;
UPDATE HumanResources.Employee 
SET VacationHours = VacationHours + 8 
FROM (SELECT TOP 10 BusinessEntityID 
      FROM HumanResources.Employee 
      ORDER BY HireDate ASC) AS th 
WHERE HumanResources.Employee.BusinessEntityID = th.BusinessEntityID; 
rollback;
-- ----------------------------------------------------------------------------
begin transaction;
SELECT * FROM Production.Product WHERE Color = N'Red';
DECLARE @NewPrice int = 10; 
UPDATE Production.Product 
SET ListPrice += @NewPrice 
WHERE Color = N'Red'; 
SELECT * FROM Production.Product WHERE Color = N'Red';
rollback;
-- ----------------------------------------------------------------------------
begin transaction;
UPDATE Production.ScrapReason 
SET Name += ' - tool malfunction' 
WHERE ScrapReasonID BETWEEN 10 and 12; 
rollback;
-- ----------------------------------------------------------------------------
begin transaction;
SELECT * FROM Production.Location WHERE CostRate > 20.00;
UPDATE Production.Location 
SET CostRate = DEFAULT 
WHERE CostRate > 20.00;
rollback;
