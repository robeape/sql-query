--Showing [AddressID], [City], [PostalCode] with postal code = 4217.
Select AddressID, City, PostalCode
	From Person.Address
	Where PostalCode = '4217'

--Showing all the postal codes (unique) in person.address.
Select DISTINCT(PostalCode)
	From Person.Address

--Showing all the postal codes (unique) in person.address and sorting them by StateProvinceID (highest to lowest).
Select DISTINCT(PostalCode), StateProvinceID
	From Person.Address
	Order By StateProvinceID DESC

--Showing Address ID, City, Modified Date, and current date as �Today�. 
--And a column with the �Year difference� between the two dates as a positive number.
Select AddressID, City, CAST(ModifiedDate as Date) as 'ModifiedDate', 
	CAST(CURRENT_TIMESTAMP as Date) as 'Today',
	DATEDIFF (YEAR, ModifiedDate, CURRENT_TIMESTAMP) as 'Year Difference'
	From Person.Address

--Showing AddressID, AddressLine1, AddressLine2, and City of the persons who live in Portland or Paris or Melton 
--and sorting by addressline1.
Select AddressID, AddressLine1, AddressLine2, City 
	From Person.Address
	Where City in ('Portland', 'Paris', 'Melton')
	Order By AddressLine1

--Showing phone numbers and the person names in the AdventureWork2014 database (First name and last name),
--BusinessEntityID and their Email Promotions with inner Join
Select FirstName, LastName, PhoneNumber, 
	PP.BusinessEntityID, EmailPromotion
	From Person.Person as PP
	JOIN Person.PersonPhone as PPP
	ON PP.BusinessEntityID = PPP.BusinessEntityID

--Showing phone numbers starts with 5 and ends with 0175 and the person names, BusinessEntityID Email Promotions. 
--Using inner Join
Select FirstName, LastName, PhoneNumber, 
	PP.BusinessEntityID, EmailPromotion
	From Person.Person as PP
	JOIN Person.PersonPhone as PPP
	ON PP.BusinessEntityID = PPP.BusinessEntityID
	WHERE PhoneNumber like '5%0175'

--Showing phone numbers starts with 5 and ends with 0175 and the person names, BusinessEntityID Email Promotions. 
--Using SUB Query.
Select FirstName, LastName, 
	BusinessEntityID, EmailPromotion
	From Person.Person 
	Where BusinessEntityID IN (Select BusinessEntityID
									From Person.PersonPhone 
									Where PhoneNumber like '5%0175')
