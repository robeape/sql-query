USE Subqueries;

SELECT *
	FROM Sales
	ORDER BY SalesID DESC;

/*--------------------------------------------*/
/*-------------------INSERT-------------------*/
/*--------------------------------------------*/

INSERT INTO Sales (ProductID, Price, SalesDate, EmpID)
VALUES (733, 356.898, GETDATE(), 17);

INSERT INTO Sales
VALUES (733, 356.898, GETDATE(), 15);

--Generated automaticaly the SalesID (Auto increment):  identity
--is not possible to change the identity when is set OFF
INSERT INTO Sales (SalesID, ProductID, Price, SalesDate, EmpID)
VALUES (64, 733, 356.898, GETDATE(), 17);

INSERT INTO Sales (ProductID, Price, EmpID)
VALUES (733, 356.898, 16);

/*--------------------------------------------*/
/*-------------------UPDATE-------------------*/
/*--------------------------------------------*/

UPDATE Sales
set EmpID = 15
where SalesID = 53;

--update several rows
UPDATE Sales
set EmpID = 15
where EmpID = 33;

/*--------------------------------------------*/
/*-------------------DELETE-------------------*/
/*--------------------------------------------*/

DELETE FROM Sales
WHERE SalesID > 50;

/* TRUNCATE faster and more efficient but will erase everything include the pointers/memory
TRUNCATE TABLE Sales */

/*--------------------------------------------*/
/*-------------------CREATE-------------------*/
/*--------------------------------------------*/

USE AdventureWorks2014
CREATE TABLE Colors (
	ColorID INT,
	Color NVARCHAR(15)
);

INSERT INTO Colors
VALUES (1, 'Pink'),
	(5, 'Blue'),
	(3, 'Purple')

SELECT * FROM Colors

DROP TABLE Colors

CREATE TABLE dbo.Colors (
	ColorID int IDENTITY NOT NULL PRIMARY KEY,
	Color nvarchar(15)
);

SET IDENTITY_INSERT dbo.colors ON

INSERT INTO Colors (ColorID, Color)
VALUES (100, 'red'),
	(200, 'Green'),
	(1, 'Blue')

DELETE FROM Colors

SET IDENTITY_INSERT dbo.colors OFF

INSERT INTO Colors
VALUES ('red'),
	('Green'),
	('Blue')

DELETE FROM Colors

INSERT INTO Colors
SELECT DISTINCT Color
FROM Production.Product
WHERE Color IS NOT NULL

TRUNCATE TABLE Colors

INSERT INTO Colors
SELECT DISTINCT Color
FROM Production.Product
WHERE Color IS NOT NULL

SELECT * 
INTO Person.Person_Backup
FROM Person.Person

SELECT LastName, FirstName
INTO Person.Person_Backup1
FROM Person.Person
WHERE Title = 'Mr.'

CREATE TABLE dbo.SickLeave (
	EmployeeID int,
	FullName nvarchar(100),
	SickLeaveHours int
)

INSERT TOP (10) PERCENT SickLeave
SELECT Person.Person.BusinessEntityID, 
	FirstName + ' ' + LastName,
	SickLeaveHours
	FROM Person.Person JOIN
		HumanResources.Employee
	ON Person.Person.BusinessEntityID = 
		HumanResources.Employee.BusinessEntityID
	ORDER BY SickLeaveHours DESC

SELECT * FROM SickLeave

INSERT SickLeave
SELECT TOP (10) PERCENT Person.Person.BusinessEntityID, 
	FirstName + ' ' + LastName,
	SickLeaveHours
	FROM Person.Person JOIN
		HumanResources.Employee
	ON Person.Person.BusinessEntityID = 
		HumanResources.Employee.BusinessEntityID
	ORDER BY SickLeaveHours DESC

SELECT * FROM SickLeave
ORDER BY SickLeaveHours DESC

--VARIABLES
DECLARE @X1 INT, @X2 INT
SELECT @X1 = 40
SELECT @X2 = 60
SELECT @X1 + @X2 AS SUM

PRINT 'This message will be displayed in the messages'
PRINT @X2;

--GO STATEMENT - after the Go command, the memory will release the variables (but not for databases).
USE AdventureWorks2014
GO
DECLARE @MyMsg1 VARCHAR(50)
DECLARE @MyMsg2 VARCHAR(50)

SELECT @MyMsg1 = 'Hello World.'
PRINT @MyMsg1
SELECT @MyMsg2 = 'Hello, College Students.'
--GO

PRINT @MyMsg2
GO

/*CHAR (fixed)
VARCHAR (the rest of the memory not used will be free for use)
NVARCHAR (the code can be used in another codes)*/

CREATE TABLE Colors (
	ColorID INT,
	Color NVARCHAR(15)
);