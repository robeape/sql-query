Use SIS;
DROP TABLE IF EXISTS mazi_table

CREATE TABLE order_item 
 ( 
  invoice_number INT NOT NULL, 
  product_id VARCHAR(11) NOT NULL, 
  quantity INT, 
  discount NUMERIC(3,3) /* optional comma missing */ 
 CONSTRAINT order_item_pk PRIMARY KEY ( invoice_number, product_id ) 
 )

CREATE DATABASE Teste
Use Teste;

CREATE TABLE product 
 ( 
	product_id VARCHAR(11) NOT NULL CONSTRAINT product_pk PRIMARY KEY, 
	description VARCHAR(75), 
	vendor_id VARCHAR(4) NOT NULL, 
	vendor_part_number VARCHAR(20) NOT NULL, 
	price NUMERIC(7,2) CONSTRAINT price_default DEFAULT 0.00, 
	reorder_threshold INT CONSTRAINT threshold_gt_zero_ck CHECK (reorder_threshold > 0 ), 
	product_category_code CHAR(1) CONSTRAINT valid_category_ck 
				   CHECK (product_category_code IN ('S','H','U','Y','P','M') ) 
)

CREATE TABLE TestTable 
( x INTEGER UNIQUE, 
y VARCHAR(20) 
); 

CREATE TABLE SecondTestTable
( x INTEGER, 
FOREIGN KEY (x) REFERENCES TestTable(x) 
ON DELETE NO ACTION 
); 

INSERT INTO TestTable VALUES(1, 'Fred');
INSERT INTO SecondTestTable VALUES( 1 ); 
INSERT INTO SecondTestTable VALUES( 2 ); 

CREATE TABLE order_item 
 ( 
	 invoice_number INT NOT NULL, 
	 product_id VARCHAR(11) NOT NULL, 
	 quantity INT, 
	 discount NUMERIC(3,3), 
	 CONSTRAINT item_header_fk FOREIGN KEY ( invoice_number ) 
	 REFERENCES order_header ( invoice_number ) ON DELETE CASCADE, 
	 CONSTRAINT item_product_fk  FOREIGN KEY ( product_id ) 
	 REFERENCES product ( product_id )  ON DELETE CASCADE, 
	 CONSTRAINT order_item_pk  PRIMARY KEY ( invoice_number, product_id ) 
 )

USE AdventureWorks2014;

CREATE TABLE Production.Products (
productid INT NOT NULL IDENTITY,
productname NVARCHAR(40) NOT NULL,
supplierid INT NOT NULL,
categoryid INT NOT NULL,
unitprice MONEY NOT NULL
CONSTRAINT DFT_Products_unitprice DEFAULT(0),
discontinued BIT NOT NULL
CONSTRAINT DFT_Products_discontinued DEFAULT(0),
CONSTRAINT PK_Products PRIMARY KEY(productid),
CONSTRAINT FK_Products_Categories FOREIGN KEY(categoryid)
REFERENCES Production.ProductCategory(Productcategoryid),
CONSTRAINT CHK_Products_unitprice CHECK(unitprice >= 0));

SELECT productname FROM Production.Products
WHERE productid = 1;

SET IDENTITY_INSERT Production.Products ON;
GO

INSERT INTO Production.Products (productid, productname, supplierid, categoryid,
unitprice, discontinued)
VALUES (1, N'Product TEST', 1, 1, 18, 0);
GO

SET IDENTITY_INSERT Production.Products OFF;

INSERT INTO Production.Products (productname, supplierid, categoryid, unitprice,discontinued)
VALUES (N'Product TEST', 1, 1, 18, 0);

DELETE FROM Production.Products WHERE categoryid = 99;

SELECT productname FROM Production.Products
WHERE productname = 'Product TEST';

INSERT INTO Production.Products (productname, supplierid, categoryid, unitprice,
			discontinued)
VALUES (N'Product TEST', 1, 99, 18, 0);
GO

--dropping the foreign key constraint on Production.Products
ALTER TABLE Production.Products
DROP CONSTRAINT FK_Products_Categories;

INSERT INTO Production.Products (productname, supplierid, categoryid, unitprice, discontinued)
VALUES (N'Product TEST', 1, 99, 18, 0);

SELECT * FROM Production.Products;

ALTER TABLE Production.Products WITH CHECK
ADD CONSTRAINT FK_Products_Categories FOREIGN KEY(categoryid)
REFERENCES Production.ProductCategory (ProductCategoryID);
GO


Select ProductID, ProductSubcategoryID
From Production.Product
Where ProductSubcategoryID NOT IN (select ProductSubcategoryID
									from Production.ProductCategory) 

UPDATE Production.Products
SET categoryid = 1
WHERE productname = N'Product TEST';
GO

DELETE FROM Production.Products 
WHERE productname = N'Product TEST';

