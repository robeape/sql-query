CREATE DATABASE "Week 12 Lab"
--creating and using db
USE "Week 12 Lab"

GO
--creating table customers
CREATE TABLE Customers 
 ( 
	 CustomerId INT IDENTITY NOT NULL, 
	 Name VARCHAR(100) NOT NULL, 
	 Country VARCHAR(50) NOT NULL, 
	 CONSTRAINT CustomerId_pk  PRIMARY KEY ( CustomerId ) 
 )

GO
--creating table for logs
CREATE TABLE CustomerLogs 
 ( 
	 LogId INT IDENTITY NOT NULL, 
	 CustomerId INT NOT NULL, 
	 Action VARCHAR(50) NOT NULL, 
	 CONSTRAINT LogId_pk  PRIMARY KEY ( LogId ) 
 )

GO
--trigger for insert commands (every time of an insert - insert a log row into CustomerLogs)
CREATE TRIGGER [dbo].[Customer_INSERT]
       ON [dbo].[Customers]
AFTER INSERT
AS
BEGIN
       SET NOCOUNT ON;  -- for higher performance
 
       DECLARE @CustomerId INT
 
       SELECT @CustomerId = INSERTED.CustomerId       
       FROM INSERTED
 
       INSERT INTO CustomerLogs
       VALUES(@CustomerId, 'Inserted')
END

GO
--trigger for delete commands (every time of a delete - insert a log row into CustomerLogs)
CREATE TRIGGER [dbo].[Customer_DELETE]
       ON [dbo].[Customers]
AFTER DELETE
AS
BEGIN
       SET NOCOUNT ON;
 
       DECLARE @CustomerId INT
 
       SELECT @CustomerId = DELETED.CustomerId       
       FROM DELETED
 
       INSERT INTO CustomerLogs
       VALUES(@CustomerId, 'Deleted')
END

GO
--insert commands for customers
INSERT INTO Customers VALUES('Roberta', 'Brazil')
INSERT INTO Customers VALUES('Chinmay', 'India')
INSERT INTO Customers VALUES('Brandon', 'Barbados')
INSERT INTO Customers VALUES('Thao', 'Vietna')
INSERT INTO Customers VALUES('May', 'Jordania')
INSERT INTO Customers VALUES('Nicholas', 'Canada')
INSERT INTO Customers VALUES('Joji', 'Japan')
INSERT INTO Customers VALUES('Luciana', 'Argentina')

--delete commands for customers
DELETE FROM Customers WHERE Name = 'Roberta'
DELETE FROM Customers WHERE Name = 'Chinmay'
DELETE FROM Customers WHERE Name = 'Brandon'

GO
--creating a view to show both tables
CREATE VIEW vw_CustomersHistory
AS
	SELECT CL.LogId, CL.CustomerId, CL.Action,
		 C.Name, C.Country
	FROM CustomerLogs AS CL
	FULL OUTER JOIN Customers AS C
	ON CL.CustomerId = C.CustomerId

GO
--select view to show logs
SELECT *
FROM vw_CustomersHistory

SP_helpText vw_CustomersHistory

select * from Customers

select * from CustomerLogs