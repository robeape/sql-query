SELECT * FROM Person.Person;
GO

SELECT 'Rafael' + NULL;
GO

--default is ON
SET CONCAT_NULL_YIELDS_NULL OFF;
GO
SELECT 'Rafael' + NULL;
GO

SET CONCAT_NULL_YIELDS_NULL ON;
GO

SELECT FirstName, LastName, title
FROM Person.Person
WHERE title is null;

SELECT TOP 10 FirstName, LastName, title
FROM Person.Person
WHERE title is null;

SELECT TOP 10 FirstName, LastName
FROM Person.Person
WHERE title is null;

SELECT TOP 10 FirstName, LastName
FROM Person.Person
WHERE title is null
ORDER BY LastName;


SELECT FirstName, LastName, title
FROM Person.Person
WHERE title is not null;

SELECT TOP 10 FirstName, LastName, title
FROM Person.Person
WHERE title is not null;

SELECT TOP 10 FirstName, LastName
FROM Person.Person
WHERE title is not null;

SELECT TOP 10 FirstName, LastName
FROM Person.Person
WHERE title is not null
ORDER BY LastName;

SELECT TOP 10 FirstName, LastName
FROM Person.Person
WHERE title = null
ORDER BY LastName;


SELECT FirstName, LastName
FROM Person.Person
WHERE FirstName = 'Hazem' OR FirstName = 'sam';

SELECT FirstName, LastName
FROM Person.Person
WHERE FirstName IN ('Hazem', 'sam', 'Humberto');


SELECT *
FROM Sales.SalesTaxRate
WHERE TaxRate BETWEEN 8 AND 9;


SELECT *
FROM [Sales].[Store]
WHERE name = 'Retreat Inn';

SELECT *
FROM [Sales].[Store]
WHERE name like 'Retreat Inn';

SELECT *
FROM [Sales].[Store]
WHERE name like 'Retreat _nn';


SELECT *
FROM [Sales].[Store]
WHERE name like 'Retreat%';

SELECT *
FROM [Sales].[Store]
WHERE name like '%Retreat%';

SELECT FirstName, LastName
FROM Person.Person
WHERE FirstName like 'R%';

SELECT FirstName, LastName
FROM Person.Person
WHERE FirstName like 'A%p%n';

SELECT *
FROM [Sales].[Store]
WHERE name NOT like '%a%' and name NOT like '%E%';

SELECT *
FROM [Sales].[Store]
WHERE name like '%a%';

SELECT *
FROM [Sales].[Store]
WHERE name NOT like '%a%' OR name NOT like '%E%';


SELECT SalesOrderID, OrderQty, ModifiedDate, ModifiedDate + 10
FROM Sales.SalesOrderDetail
ORDER BY OrderQty;


SELECT SalesOrderID, OrderQty, ModifiedDate, ModifiedDate + 0.5
FROM Sales.SalesOrderDetail
ORDER BY OrderQty;


SELECT GETDATE() - '2000-01-01' AS 'Days since the millenium';

SELECT 9.5 AS 'Original';

SELECT CAST(9.5 AS int) AS int;

SELECT CAST(9.5 AS decimal(6,4)) AS decimal;

SELECT CAST(19.5 AS decimal(6,4)) AS decimal;

SELECT CAST(19.50000 AS decimal(6,4)) AS decimal;

SELECT CAST(GETDATE() - '2000-01-01' AS INT) AS 'Days since the millenium';

SELECT GETDATE();

SELECT DATEPART(mm,'2019/12/04') AS DatePartInt;

SELECT DATEPART(dd,'2019/12/04') AS DatePartInt;

SELECT DATEPART(YY,'2019/12/04') AS DatePartInt;

SELECT DATEADD(year, 1, '2017/08/25') AS DateAdd;

SELECT DATEADD(month, -3, '2017/08/25') AS DateAdd;

--how many days I am living in Canada
SELECT DATEDIFF(day, '2018/12/26', GETDATE()) AS DateDiff;

SELECT CONVERT (CHAR(10),GETDATE(),102),GETDATE();

SELECT CONVERT (CHAR(10),GETDATE(),103),GETDATE();

SELECT CONVERT (CHAR(10),GETDATE(),104),GETDATE();

SELECT CONVERT (CHAR(10),GETDATE(),108),GETDATE();

SELECT CONVERT (CHAR(10),GETDATE(),3),GETDATE();
