select FirstName, LastName, PersonType
	from Person.Person
	order by FirstName;

SELECT top 10 FirstName, LastName,
	CASE PersonType
	WHEN 'SC' THEN 'Store Contact'
	WHEN 'IN' THEN 'Individual Customer'
	WHEN 'SP' THEN 'Sales Person'
	WHEN 'EM' THEN 'Employee'
	WHEN 'VC' THEN 'Vendor Contact'
	WHEN 'GC' THEN 'General Contact'
	ELSE 'Unknown Person Type'
	END AS [Type of Contact]
	FROM Person.Person
	order by FirstName;

SELECT VendorName, VendorID,
	CASE VendorState
	WHEN 'TX' THEN 'Texas'
	WHEN 'WI' THEN 'Wisconsin'
	WHEN 'NY' THEN 'New York'
	END AS [State]
	FROM Vendors
	WHERE VendorState IN ('TX', 'WI', 'NY')


